// TODO
const express = require('express')
const app = express()
const port = process.env.PORT || 56201;
app.use(express.text())

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.post('/square', function (req, res) {
    // console.log(req.body)
    const num_str = req.body;
    // console.log(num_str);
    if (!isNaN(num_str)){
        let num = +num_str;
        res.send(
            {
                number: num,
                square: num*num
            }
        )
    }
    else {
        res.send("Invalid data input")
    }

})

app.post('/reverse', function (req, res) {
    const input_str = req.body;
    // console.log(input_str);
    if (input_str){
        res.send(input_str.split('').reverse().join(''));
    }
    else {
        res.send("Error");
    }
})
app.get('/date/:year/:month/:day', (req, res) => {
    const year = parseInt(req.params.year);
    const month = parseInt(req.params.month);
    const day = parseInt(req.params.day);

    const date = new Date(year, month - 1, day); // month - 1, бо місяці в JavaScript починаються з 0
    const weekDay = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][date.getDay()];
    const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    const today = new Date();
    const difference = Math.ceil((date - today) / (1000 * 60 * 60 * 24));

    const response = {
        weekDay: weekDay,
        isLeapYear: isLeapYear,
        difference: Math.abs(difference)
    };

    res.json(response);
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})